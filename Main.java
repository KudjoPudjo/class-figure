import java.lang.reflect.Array;
import java.util.Random;

public class Main {
    public static void main(String[] args){
        final Random random = new Random();
         String figure[] = new String[5];
         for (int i = 0;i<5;i++){
            int id = random.nextInt(2);
            if(id==1){
                Circle circle = new Circle(10,9);
                int ploshad = circle.square();
                String name = circle.getName();
                String f = "Название фигуры: "+name+", Площадь "+ploshad;
                figure[i]=f;
            }else if(id == 0){
                Rectangle rectangle = new Rectangle(10,9);
                int plos = rectangle.square();
                String nam = rectangle.getName();
                String fa="Название фигуры: "+nam+", Площадь "+plos;
                figure[i]=fa;
            }
         }
         for(int i = 0; i <5;i++){
             System.out.println(figure[i]);
         }


    }
}
abstract class Figure {

    float x;
    float y;
    public Figure(float x,float y){
        this.y = y;
        this.x = x;
    }

    public abstract int square();
    public void getQuadrant(){
        if(x>0 & y>0){
            System.out.println("Центр лежит в первой плоскости");
        }else if (x<0 & y>0){
            System.out.println("Центр лежит в второй плоскости");
        }else if (x<0 & y<0){
            System.out.println("Центр лежит в третей плоскости");
        }
        else if (x>0 & y<0){
            System.out.println("Центр лежит в четвертой плоскости");
        }
    };
}
class Circle extends Figure{
    public Circle(int x,int y) {
        super(x,y);
    }
    final Random random = new Random();
    private int radius = random.nextInt(20);
    private String name = "Круг";
    public String getName(){
        return name;
    }
    public int square() {
        double ploshad = (radius*radius) * 3.14;
        return (int) ploshad;
    }
}
class Rectangle extends Figure{
    public Rectangle(int x,int y) {
        super(x,y);
    }
    final Random random = new Random();
    private int width = random.nextInt(20);;
    private int height = random.nextInt(20);;
    private String name = "Прямоугольник";
    public String getName(){
        return name;
    }

    public int square(){
        int ploshad = width * 2 + height * 2;
        return ploshad;
    }
}